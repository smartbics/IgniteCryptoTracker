import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../../../Themes/'

export default StyleSheet.create({
  itemList: {
    flex: 1,
    margin: 10,
    marginBottom: 0,
    width: '80%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 4,
    backgroundColor: '#2D2E34'
  },
  coin: {
    fontSize: 20,
    fontWeight: '600',
    margin: 10,
    marginBottom: 0,
    fontFamily: Fonts.type.numbers,
    textAlign: 'left',
    color: Colors.white
  },
  count: {
    fontSize: 16,
    margin: 10,
    marginTop: 0,
    fontWeight: '600',
    textAlign: 'left',
    fontFamily: Fonts.type.numbers,
    color: '#515767'
  },
  price: {
    fontSize: 20,
    margin: 10,
    marginBottom: 0,
    fontWeight: '600',
    fontFamily: Fonts.type.numbers,
    textAlign: 'right',
    color: Colors.white
  },
  position: {
    margin: 10,
    color: '#515767',
    textAlign: 'center',
    marginTop: 0,
    fontSize: 16
  }
})
