import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../../../Themes/'

export default StyleSheet.create({
  list: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  itemList: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderBottomWidth: 2,
    height: 60,
    borderBottomColor: Colors.black
  },
  count: {
    padding: 10,
    fontSize: 18,
    height: 44,
    flexBasis: '20%',
    fontWeight: '600',
    fontFamily: Fonts.type.numbers,
    flex: 1,
    textAlign: 'center'
  },
  coin: {
    padding: 10,
    fontSize: 18,
    height: 44,
    flexBasis: '20%',
    fontWeight: '600',
    fontFamily: 'Roboto',
    fontStyle: 'italic',
    flex: 1,
    textAlign: 'center'
  },
  data: {
    fontFamily: Fonts.type.base,
    padding: 10,
    fontSize: 14,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: '300',
    fontStyle: 'italic',
    flexBasis: '25%',
    textAlign: 'center'
  },
  buy: {
    padding: 10,
    fontSize: 18,
    height: 44,
    flexBasis: '20%',
    color: Colors.green,
    textAlign: 'center'
  },
  sell: {
    padding: 10,
    fontSize: 18,
    height: 44,
    flexBasis: '20%',
    color: Colors.fire,
    textAlign: 'center'
  },
  arrow: {
    padding: 10,
    flexBasis: '15%',
    color: Colors.black,
    textAlign: 'center'
  }
})
