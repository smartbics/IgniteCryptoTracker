import React from 'react'
import { Text, View } from 'react-native'
import { portfolio } from './Styles'


export default function PortfolioItem(item) {
  return (
    <View style={portfolio.itemList}>
      <View style={{ flexDirection: 'column', justifyContent: 'flex-start' }}>
        <Text style={portfolio.coin}>{item.coin}</Text>
        <Text style={portfolio.count}>{`${item.quantity}/${item.price} €`}</Text>
      </View>
      <View style={{ flexDirection: 'column', justifyContent: 'flex-end' }}>
        <Text style={portfolio.price}>{`${item.totalSum} €`}</Text>
        <Text style={portfolio.Position}>{item.changeFromPurchase > 0 ? `+ ${item.changeFromPurchase}%` : `- ${item.changeFromPurchase}%`}</Text>
      </View>
    </View>)
}
