import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { transaction } from './Styles'


export default function TransactionHistoryItem(item) {
  return (
    <TouchableOpacity onPress={item.onPress} >
      <View style={transaction.itemList}>
        <Text style={transaction.data}>{item.date}</Text>
        <Text style={transaction[String(item.transactionType.toLowerCase())]}>
          {item.transactionType}
        </Text>
        <Text style={transaction.coin}>{item.targetCoin}</Text>
        <Text style={transaction.count}>{item.targetCoinQuantity}</Text>
        <Icon style={transaction.arrow} name="angle-right" size={20} />
      </View>
    </TouchableOpacity>)
}
