import React from 'react'
import PropTypes from 'prop-types';
import { FlatList } from 'react-native'

export default function ICTList(props) {
  const { data, ItemTemplate, TitleTemplate } = props
  return (
    <FlatList
      data={data}
      renderItem={ItemTemplate}
      ListHeaderComponent={TitleTemplate}
    />
  )
}

ICTList.propTypes = {
  data: PropTypes.array.isRequired,
  ItemTemplate: PropTypes.func.isRequired,
  TitleTemplate: PropTypes.func.isRequired
}
