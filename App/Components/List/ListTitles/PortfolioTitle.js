import React from 'react'
import { Text, View } from 'react-native'
import { portfolio } from './Styles'

export default function PortfolioTitle() {
  return (
    <View style={portfolio.titleList}>
      <Text style={portfolio.title}>Name</Text>
      <Text style={portfolio.title}>Holdings/Value</Text>
      <Text style={portfolio.title}>Price</Text>
      <Text style={portfolio.title}>Change</Text>
    </View>
  )
}
