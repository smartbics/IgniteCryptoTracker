import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../../../Themes/'

export default StyleSheet.create({
  titleList: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: 2,
    height: 60,
    borderTopColor: Colors.black
  },
  title: {
    fontFamily: Fonts.type.number,
    flexBasis: '25%',
    textAlign: 'center'
  }
})
