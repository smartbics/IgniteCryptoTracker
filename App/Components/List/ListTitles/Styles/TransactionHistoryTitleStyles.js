import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../../../Themes/'

export default StyleSheet.create({
  list: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  titleDate: {
    padding: 10,
    fontSize: 18,
    fontWeight: '600',
    fontFamily: Fonts.type.base,
    flexBasis: '25%',
    textAlign: 'center'
  },
  title: {
    padding: 10,
    fontSize: 18,
    fontWeight: '600',
    fontFamily: Fonts.type.base,
    flexBasis: '20%',
    textAlign: 'center'
  },
  arrow: {
    padding: 10,
    flexBasis: '15%',
    color: Colors.black,
    textAlign: 'center'
  }
})
