import portfolio from './PortfolioTitleStyles'
import transaction from './TransactionHistoryTitleStyles'

export {
  portfolio,
  transaction
}
