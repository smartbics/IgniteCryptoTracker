import PortfolioTitle from './PortfolioTitle'
import TransactionTitle from './TransactionHistoryTitle'

export {
  PortfolioTitle,
  TransactionTitle
}
