import React from 'react'
import { Text, View } from 'react-native'
import { transaction } from './Styles'

export default function TransactionHistoryTitle() {
  return (
    <View style={transaction.list}>
      <Text style={transaction.titleDate}>Date</Text>
      <Text style={transaction.title}>Type</Text>
      <Text style={transaction.title}>Coin</Text>
      <Text style={transaction.title}>Count</Text>
      <Text style={transaction.arrow} />
    </View>
  )
}
