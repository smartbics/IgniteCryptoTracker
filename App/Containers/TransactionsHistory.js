import React from 'react'
import { Text, View } from 'react-native'
import { values } from 'lodash'
import { TransactionTitle } from '../Components/List/ListTitles'
import { TransactionItem } from '../Components/List/ListItems'
import ICTList from '../Components/List/ICTList'
import styles from './Styles/TransactionsHistoryStyles'

const input = {
  1: {
    id: 1,
    date: '23.03.2018',
    transactionType: 'Buy',
    targetCoin: 'ETH',
    targetCoinQuantity: 2.0
  },
  2: {
    id: 2,
    date: '23.03.2018',
    transactionType: 'Sell',
    targetCoin: 'ETH',
    targetCoinQuantity: 1.5
  }
}

export default function TransactionsHistory() {
  const data = values(input).map((el) => ({ ...el, key: el.id }))

  return (
    <View style={styles.container}>
      <Text style={styles.mainTitle}>Transactions</Text>
      <ICTList
        data={data}
        ItemTemplate={({ item }) => (TransactionItem(item))}
        TitleTemplate={TransactionTitle}
      />
    </View>
  )
}
