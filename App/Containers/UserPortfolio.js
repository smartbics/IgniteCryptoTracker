import React from 'react'
import { values } from 'lodash'
import { Text, View } from 'react-native'
import ICTList from '../Components/List/ICTList'
import { PortfolioItem } from '../Components/List/ListItems'
import styles from './Styles/UserPortfolioStyles'


const userPortfolio = {
  investments: {
    1: {
      coin: 'ETH',
      quantity: 2,
      price: 1000,
      totalSum: 2000,
      changeLastDay: 0.2,
      changeFromPurchase: 0.3
    },
    2: {
      coin: 'RDL',
      quantity: 4,
      price: 1300,
      totalSum: 13000,
      changeLastDay: -0.2,
      changeFromPurchase: 0.4
    }
  },
  totalHoldings: {
    totalSum: 2000,
    changeFromPurchase: 0.3
  }
}

export default function UserPortfolio() {
  const items = values(userPortfolio.investments).map((el, index) => ({ ...el, key: index }))
  const { changeFromPurchase, totalSum } = userPortfolio.totalHoldings;
  const value = changeFromPurchase < 0 ? `- ${changeFromPurchase}%` : `+ ${changeFromPurchase}%`
  const sum = `${totalSum} €`

  return (
    <View style={styles.container}>
      <Text style={styles.mainTitle}>Portfolio</Text>
      <View style={styles.view}>
        <Text style={styles.balanceTitle}>Balance:</Text>
        <Text style={styles.balanceValue}>{sum}</Text>
        <Text style={styles.position}>{value}</Text>
      </View>
      <Text style={styles.secondTitle}>Chart will be here </Text>
      <ICTList
        data={items}
        ItemTemplate={({ item }) => (PortfolioItem(item))}
      />
    </View>
  )
}
