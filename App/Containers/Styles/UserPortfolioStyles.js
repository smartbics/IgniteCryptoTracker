import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#222133'
  },
  mainTitle: {
    margin: 0,
    width: '100%',
    textAlign: 'center',
    padding: 10,
    fontSize: 24,
    fontWeight: '600',
    fontFamily: Fonts.type.base,
    color: Colors.white,
    backgroundColor: '#6749E5',
  },
  secondTitle: {
    margin: 0,
    textAlign: 'center',
    padding: 0,
    fontSize: 22,
    height: 40,
    fontWeight: '500',
    fontFamily: Fonts.type.base,
    color: '#123241'
  },
  balanceTitle: {
    fontSize: 22,
    marginTop: 10,
    color: Colors.white,
    fontFamily: Fonts.type.numbers
  },
  view: {
    flexDirection: 'column',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 0,
    backgroundColor: '#6749E5'
  },
  balanceValue: {
    fontSize: 32,
    padding: 5,
    fontWeight: '600',
    color: Colors.white,
    fontFamily: Fonts.type.numbers,
    marginTop: 10
  },
  position: {
    color: Colors.white,
    fontSize: 20,
    paddingBottom: 10
  },
  losePosition: {
    color: Colors.fire,
    fontSize: 36,
    fontWeight: '600',
    padding: 10
  }
})
