import { StyleSheet } from 'react-native'
import { Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  mainTitle: {
    margin: 0,
    width: '100%',
    textAlign: 'center',
    padding: 10,
    fontSize: 24,
    fontWeight: '600',
    fontFamily: Fonts.type.base,
    color: Colors.white,
    backgroundColor: Colors.green,
    borderBottomWidth: 2,
    borderBottomColor: Colors.black
  }
})
