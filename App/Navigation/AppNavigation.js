import { TabNavigator } from 'react-navigation'
// import LaunchScreen from '../Containers/LaunchScreen'
import TransactionsHistory from '../Containers/TransactionsHistory'
import UserPortfolio from '../Containers/UserPortfolio'

// import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = TabNavigator({
  TransactionsHistory: { screen: TransactionsHistory },
  UserPortfolio: { screen: UserPortfolio }
}, {
  tabBarPosition: 'bottom',
  swipeEnabled: false,
  initialRouteName: 'TransactionsHistory',
  navigationOptions: {
    // headerStyle: styles.header
  }
})

export default PrimaryNav
