import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas'

import NavigationReduxReducer from './NavigationRedux'
import { reducer as GithubReduxReducer } from './GithubRedux'
import { reducer as SearchReduxReducer } from './SearchRedux'

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: NavigationReduxReducer,
  github: GithubReduxReducer,
  search: SearchReduxReducer
})

/* eslint global-require: "off" */
/* eslint prefer-const: "off" */
export default () => {
  let { store, sagasManager, sagaMiddleware } = configureStore(reducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
